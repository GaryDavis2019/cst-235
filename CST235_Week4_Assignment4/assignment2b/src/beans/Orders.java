package beans;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.faces.bean.*;

@ManagedBean(name="orders", eager = true) 
@ViewScoped


public class Orders implements Serializable {
	   private static final long serialVersionUID = 1L;
		String orderNumber;
		String productName;
		float price;
		int quantity;
		
		//Create a list with temp data
	   private static final ArrayList<Order> orders = new ArrayList<Order>(Arrays.asList(new Order("A1234","TEST",112,10),
			   new Order("B1234","TEST2",1,120),new Order("C1234","TEST3",12,180),new Order("D1234","TEST4",11,64)));

 
	//Orders getter/setter	
	public List<Order> getOrders() {
		return orders;
	}
	public void setOrderNumber(Order OrderObj) {
		orders.add(OrderObj);
	}
	
}
